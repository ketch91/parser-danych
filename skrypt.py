# -*- coding: utf-8 -*-
"""
Spyder Editor

This temporary script file is located here:
/home/piotr/.spyder2/.temp.py


import csv

import rdflib
import csv
from rdflib import Graph

g= Graph()
"""
import sitecustomize
import numpy as nump
import matplotlib.pyplot as plot

csvArray = []
vectorsMovies = []
tempVectorString = []
procMatrix = []

"""
Funkcja odcinająca pewien przedział z listy
"""
def cut(lista, procent):
    rozmiar = len(lista)
    usun = (rozmiar*procent)/100
    return lista[-usun:]

"""
Funkcja na obliczanie współczynnika Ginniego
"""
def countGini(list_of_values):
    sorted_list = sorted(list_of_values)
    height, area = 0, 0
    for value in sorted_list:
        height += value
        area += height - value / 2.
    fair_area = height * len(list_of_values) / 2
    return (fair_area - area) / fair_area

"""
Otwarcie pliku u.data
"""
ins = open( "u.data", "rw" )

"""
Pobieranie do tablicy

csvArray [ id_uzytkownika, id_filmu, ocena, czas ]
"""
for line in ins:
    line = line.replace("\n", "")
    line = line.split('\t')
    csvArray.append( line )

"""
Rzutowanie na int
"""
for i in range(100000):
    for j in range(3):
        csvArray[i][j] = int(csvArray[i][j])

"""
Nadanie wartości początkowych
"""
maxMovieId = csvArray[0][1]
minMovieId = csvArray[0][1]
   
"""
Wyszukanie MIN oraz MAX
"""     
for i in range(100000):
    if csvArray[i][1] > maxMovieId:
        maxMovieId = csvArray[i][1]
        
    if csvArray[i][1] < minMovieId:
        minMovieId = csvArray[i][1]
      
"""
Budowanie wektorów dla filmów

vectorsMovies [ id_filmu, suma_ocen, liczba_ocen, średnia_ocen ]
range 100 000
"""    
for currentId in range(minMovieId,maxMovieId):
    tempSum = 0
    tempRateCount = 0
    tempAvg = 0
    
    for i in range(100000):
        if csvArray[i][1] == currentId:
            tempSum = tempSum + csvArray[i][2]
            tempRateCount = tempRateCount + 1
            tempAvg = tempSum / tempRateCount
            
    tempVectorString = [currentId, tempSum, tempRateCount, tempAvg]
    vectorsMovies.append( tempVectorString )


"""
Sortowanie wektora po x[2] ( liczba_ocen )
"""  
vectorsMovies.sort(key=lambda x: x[2])

vectorsMoviesInt = nump.asarray(vectorsMovies)

"""
Kolumny na potrzeby wykresu
x = id-filmu
y = liczba wystapien
"""  
x = vectorsMoviesInt[:,0]
y = vectorsMoviesInt[:,1]

"""
Sortowanie tablicy z liczbą ocen
"""
y.sort()

plot1 = plot.figure()
plot.plot(x, y, 'bo')
plot1.suptitle('Stosunek ilości filmów do liczby ocen', fontsize=20)
plot.xlabel('Liczba filmow', fontsize=16)
plot.ylabel('Liczba ocen', fontsize=16)
#fig.savefig('test.svg')

"""
Krzywa Lorenza
"""
FilmsSum = 0
c = len(y)
for i in range(0, c):
    procMatrix.append(FilmsSum)
    FilmsSum += y[i]

reveseY = sorted(y, reverse=True)
reverseA = nump.asanyarray(reveseY)
interval = cut(reverseA, 80)

print countGini(interval)

#print(tempVectorString)
""" 

plt.plot([1, 1, 1, 1])
""" 
"""
print(vectorsMovies[len(vectorsMovies)-4])
print(vectorsMovies[len(vectorsMovies)-3])
print(vectorsMovies[len(vectorsMovies)-2])
print(vectorsMovies[len(vectorsMovies)-1])


for vectorsMovies in g:
        print(vectorsMovies)
"""   
"""    
print(csvArray[1])
"""