# -*- coding: utf-8 -*-
"""
Spyder Editor

This temporary script file is located here:
/home/piotr/.spyder2/.temp.py


import csv

import rdflib
import csv
from rdflib import Graph

g= Graph()
"""
import matplotlib.pyplot as plt
import numpy as np

csvArray = []
vectorsMovies = []
vectorsMoviesConverted = []
x = []
y = []
tempVectorString = []

"""
Otwarcie pliku u.data
"""
ins = open( "u.data", "rw" )

"""
Pobieranie do tablicy

csvArray [ id_uzytkownika, id_filmu, ocena, czas ]
"""
for line in ins:
    line = line.replace("\n", "")
    line = line.split('\t')
    csvArray.append( line )

"""
Rzutowanie na int
"""
for i in range(100000):
    for j in range(3):
        csvArray[i][j] = int(csvArray[i][j])

"""
Nadanie wartości początkowych
"""
maxMovieId = csvArray[0][1]
minMovieId = csvArray[0][1]
   
"""
Wyszukanie MIN oraz MAX
"""     
for i in range(100000):
    if csvArray[i][1] > maxMovieId:
        maxMovieId = csvArray[i][1]
        
    if csvArray[i][1] < minMovieId:
        minMovieId = csvArray[i][1]
      
"""
Budowanie wektorów dla filmów

vectorsMovies [ id_filmu, suma_ocen, liczba_ocen, średnia_ocen ]
range 100 000
"""    
for currentId in range(minMovieId,maxMovieId):
    tempSum = 0
    tempRateCount = 0
    tempAvg = 0
    
    for i in range(100000):
        if csvArray[i][1] == currentId:
            tempSum = tempSum + csvArray[i][2]
            tempRateCount = tempRateCount + 1
            tempAvg = tempSum / tempRateCount
            
    tempVectorString = [currentId, tempSum, tempRateCount, tempAvg]
    vectorsMovies.append( tempVectorString )

"""
Sortowanie wektora vectorsMovies po tempRateCount
"""  
vectorsMovies.sort(key=lambda x: x[2])

"""
Konwersja na tablicę
""" 
vectorsMoviesConverted = np.asarray(vectorsMovies)

"""
Pocięcie tablicy na x oraz y
""" 
x = vectorsMoviesConverted[:, 0]
y = vectorsMoviesConverted[:, 2]

"""
Sortowanie elementów na potrzeby wykresu
( zakłamanie ID potraktowany jako iterator od 0 do 1681)
""" 
x.sort()
y.sort()

plt.plot(x, y)
